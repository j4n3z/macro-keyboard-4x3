# Macro keyboard 4x3

## O projektu

Cílem bylo vytvořit si vlastní makro klávesnici, která bude sloužit k ovládání OBS a dalším zkratkám v systému. Firmware je čistě QMK s vlastní konfigurací přímo ušitou na míru mému hardware.

## Seznam dílů

- 1x Raspberry Pi Pico (H)
- 12x Cherry MX black
- 1x deska plošného spoje
- 12x usměrňovací dioda 1N4148